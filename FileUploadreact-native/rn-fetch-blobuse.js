import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import {Container, Content, Left, Button, Text, Right} from 'native-base';
import Textarea from 'react-native-textarea';
import Amplify, {Auth, Hub, Storage} from 'aws-amplify';
import compose from 'lodash.flowright';
import {graphql, Query, withApollo} from 'react-apollo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  ActivityIndicator,
  View,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import StyledText from '../../components/StyledText';
import Headers from '../../components/Header';
import Tumbnail from '../../components/ImageLoader';
import PostCreate from '../../Appsync/mutation/post/createpost';
import Getdata from '../../Appsync/query/Auth/getdata';
import awsexport from '../../aws-exports';
import moment from 'moment';
import NetInfo from '@react-native-community/netinfo';
import NoInternet from '../../components/Internet';
import AWS from 'aws-sdk/dist/aws-sdk-react-native';
import RNFetchBlob from 'rn-fetch-blob';
import {RNS3} from 'react-native-s3-upload';
import MediaControls, {PLAYER_STATES} from 'react-native-media-controls';
const screenWidth = Math.round(Dimensions.get('window').width);
import Video from '../../components/react-native-af-video-player-master';
import FormData from 'form-data';
import axios from 'axios';
import fileType from 'react-native-file-type';

class AddPost extends Component {
  constructor() {
    super();
    this.state = {
      posttext: '',
      userdata: {},
      ImageSource: null,
      VideoSource: null,
      post: false,
      loading: true,
      connected: true,
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: 'content',
      visible: false,
      username: '',
      loader: false,
    };
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    this.selectVideoTapped = this.selectVideoTapped.bind(this);
  }
;


  createpost = () => {
      if (this.state.ImageSource) {
        var type = this.state.ImageSource.type;
        const {bucket, aws_project_region} = awsexport;
        const key = `post/${this.state.userdata.id}/${this.state.ImageSource.name}`;
        const url = `https://${bucket}.s3.${aws_project_region}.amazonaws.com/public/${key}`;
        Storage.put(key, new Buffer(this.state.ImageSource.data, 'base64'), {
          contentType: type,
        })
          .then((result) => {
            // console.log(url);
            this.submitpost(url);
          })
          .catch((err) => console.log(err));
      } 
      if (this.state.VideoSource) {
        const {
          bucket,
          aws_cognito_region,
          acsess_key,
          secret_access_key,
        } = awsexport;
        const access = new AWS.Credentials({
          accessKeyId: acsess_key,
          secretAccessKey: secret_access_key,
        });
    
        const s3 = new AWS.S3({
          credentials: access,
          region: aws_cognito_region,
        });
        var key = `public/post/${this.state.username}/${this.state.VideoSource.name}`;
        const s3Url = await s3.getSignedUrl('putObject', {
          Bucket: bucket,
          Key: key,
          ContentType: this.state.VideoSource.mime,
        });
        RNFetchBlob.fetch(
          'PUT',
          s3Url,
          {'Content-Type': this.state.VideoSource.type},
          Platform.OS === 'ios'
            ? RNFetchBlob.wrap(this.state.VideoSource.uri)
            : `RNFetchBlob-file://${this.state.VideoSource.uri}`,
        )
          .then((resp) => {
            console.log(resp.text());
            
          })
          .catch((err) => {
            console.log(err);
          });
  try {
    var type = this.state.VideoSource.name.split('.');
    type = type[1];
    const file = {
      // `uri` can also be a file system path (i.e. file://)
      uri: this.state.VideoSource.uri,
      name: this.state.VideoSource.name,
      type: type,
    };
  const {
    bucket,
    aws_cognito_region,
    acsess_key,
    secret_access_key,
  } = awsexport;
  var key = `public/post/${this.state.username}/${this.state.VideoSource.name}`;
    const options = {
      keyPrefix: `public/post/${this.state.username}/`,
      bucket: bucket,
      region: aws_cognito_region,
      accessKey: acsess_key,
      secretKey: secret_access_key,
      successActionStatus: 201,
    };
    try {
  RNS3.put(file, options).then((response) => {
    if (response.status == 201) {
      const url = `https://${bucket}.s3.${aws_cognito_region}.amazonaws.com/${key}`;
      var mypost = {
        authorId: this.state.userdata.id,
        content: this.state.posttext,
        postVideo: url,
        userId: this.state.username,
      };
      this.props
        .PostCreate({
          variables: mypost,
        })

        .then(({data}) => {
          console.log(data);
          this.setState({loader: false});

          if (this.state.userdata.userType == 'PRO') {
            this.props.navigation.navigate('DashBoardProf');
          } else {
            this.props.navigation.navigate('DashBoardOrg');
          }
        })
        .catch((err) => {
          console.log(`${JSON.stringify(err)}`);
        });
        } else {
          alert('Something went wrong.');
        }
      });
    } catch (err) {
      console.log(err);
    }
  } catch (e) {
    console.log(e);
    alert('Something went wrong.');
  }
      }
  };


  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        // console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      } else {
        if (response.height > response.width) {
          alert('Image Height Cannot be greater than width.');
        } else {
          // console.log(response);
          let source = {
            uri: response.uri,
            type: response.type,
            data: response.data,
            name: response.fileName,
          };

          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };

          this.setState({
            ImageSource: source,
            VideoSource: source,

          });
        }
      }
    });
  }

  selectVideoTapped() {
    const options = {
      title: 'Video Picker',
      mediaType: 'video',
      storageOptions: {
        skipBackup: true,
      },
      videoQuality: 'medium',
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        // console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      } else {
        if (response.height > response.width) {
          alert('Image Height Cannot be greater than width.');
        } else {
          var source = {};
          console.log(response);

          if (Platform.OS == 'ios') {
            console.log(response);
            source = {
              uri: response.uri,
              type: response.type,
              data: response.data,
              name: response.fileName,
            };
            this.setState({
              VideoSource: source,
            });
          } else {
            RNFetchBlob.fs
              .stat(response.path)
              .then((stats) => {
                console.log(stats);
                source = {
                  uri: stats.path,
                  data: response.data,
                  name: stats.filename,
                };
                fileType(response.path).then((type) => {
                  source['type'] = type.ext;
                  source['mime'] = type.mime;
                });
                this.setState({
                  VideoSource: source,
                });
              })
              .catch((err) => {
                console.log(err);
              });
          }

          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        }
      }
    });
  }

  removeIamge = () => {
    this.setState({
      ImageSource: null,
      VideoSource: null,
    });
  };







  render() {
    const {userdata} = this.state;
    return (
      <Container>
        <Headers
          leftIcon={
            <Left style={{width: 40}}>
              <Button
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                transparent>
                <AntDesign active size={25} color={'white'} name={'close'} />
              </Button>
            </Left>
          }
          navigation={this.props.navigation}
          title={'Create Post'}
          rightIcon={
            <Right style={{width: 40}}>
              <Button
                disabled={this.state.loader}
                onPress={() => {
                  this.createpost();
                }}
                transparent>
                <Text style={{color: 'white'}}>Post</Text>
              </Button>
            </Right>
          }
        />
        {this.state.isConnected && (
          <Content style={{padding: 10}}>

            {this.state.VideoSource === null &&
              this.state.ImageSource === null &&
              !this.state.loader && (
                <TouchableOpacity
                  onPress={this.selectPhotoTapped.bind(this)}
                  style={{
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: '#212121',
                    padding: 5,
                    flexDirection: 'row',
                    alignSelf: 'flex-start',
                  }}>
                  <AntDesign name="paperclip" size={20} color={'#212121'} />
                  <Text style={{color: '#212121', fontSize: 20, marginLeft: 5}}>
                    Image
                  </Text>
                </TouchableOpacity>
              )}

            {this.state.VideoSource !== null && !this.state.loader && (
              <View style={{padding: 5, alignItems: 'center'}}>
                <View
                  style={{
                    height: 300,
                    width: '100%',
                  }}>
                  <Video url={"https://amplify-dynamotest1-dev-172145-deployment.s3.ap-south-1.amazonaws.com/Testing/post/Google_108804521723484442679/file_example_MP4_640_3MG.mp4"} />
                </View>

                <TouchableOpacity
                  onPress={() => {
                    this.removeIamge();
                  }}
                  style={{
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: '#212121',
                    padding: 5,
                    marginTop: 10,
                    flexDirection: 'row',
                    alignSelf: 'flex-start',
                  }}>
                  <AntDesign name="close" size={20} color={'#212121'} />
                  <Text style={{color: '#212121', fontSize: 20, marginLeft: 5}}>
                    Remove
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            {this.state.ImageSource !== null && !this.state.loader && (
              <View style={{padding: 5, alignItems: 'center'}}>
                <Image
                  style={{
                    height: 300,
                    width: '100%',
                    borderWidth: 1,
                    borderRadius: 10,
                  }}
                  source={this.state.ImageSource}
                />
                <TouchableOpacity
                  onPress={() => {
                    this.removeIamge();
                  }}
                  style={{
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: '#212121',
                    padding: 5,
                    marginTop: 10,
                    flexDirection: 'row',
                    alignSelf: 'flex-start',
                  }}>
                  <AntDesign name="close" size={20} color={'#212121'} />
                  <Text style={{color: '#212121', fontSize: 20, marginLeft: 5}}>
                    Remove
                  </Text>
                </TouchableOpacity>
              </View>
            )}

            {this.state.VideoSource === null &&
              this.state.ImageSource === null &&
              !this.state.loader && (
                <TouchableOpacity
                  onPress={this.selectVideoTapped.bind(this)}
                  style={{
                    borderWidth: 1,
                    borderRadius: 5,
                    borderColor: '#212121',
                    padding: 5,
                    marginVertical: 10,
                    flexDirection: 'row',
                    alignSelf: 'flex-start',
                  }}>
                  <AntDesign name="paperclip" size={20} color={'#212121'} />
                  <Text style={{color: '#212121', fontSize: 20, marginLeft: 5}}>
                    Video
                  </Text>
                </TouchableOpacity>
              )}
            {this.state.loader && (
              <ActivityIndicator size="large" color="#00ff00" />
            )}
          </Content>
        )}
        
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 20,
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: '#212121',
  },
});
const Login = compose(
  withApollo,
  graphql(PostCreate, {name: 'PostCreate'}),
)(AddPost);

export default Login;
