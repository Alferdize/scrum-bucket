testit = async (token) => {
    const {
      bucket,
      aws_cognito_region,
      acsess_key,
      secret_access_key,
    } = awsexport;
    var key = `public/post/${this.state.username}/${this.state.VideoSource.name}`;
    console.log(key, this.state.VideoSource);
    try {
      RNFetchBlob.fetch(
        'POST',
        `https://${bucket}.s3.${aws_cognito_region}.amazonaws.com/public/${key}`,
        {
          // dropbox upload headers
          Accept: 'application/json',
          Authorization: token,
          'Content-Type': 'multipart/form-data',
          // Change BASE64 encoded data to a file path with prefix `RNFetchBlob-file://`.
          // Or simply wrap the file path with RNFetchBlob.wrap().
        },
        [
          {
            name: 'information',
            filename: this.state.VideoSource.name,
            // use custom MIME type
            type: this.state.VideoSource.mime,
            // upload a file from asset is also possible in version >= 0.6.2
            data: RNFetchBlob.wrap(this.state.VideoSource.uri),
          },
        ],
      )
        .uploadProgress((written, total) => {
          console.log('uploaded', written / total);
        })
        .then((res) => {
          console.log(res.text());
        })
        .catch((err) => {
          console.log(err);
        });
      // console.log(result)
    } catch (e) {
      console.error('Error', e);
    }
  }

    // createpost = () => {
  //   Auth.currentSession().then((res) => {
  //     let accessToken = res.getAccessToken();
  //     let jwt = accessToken.getJwtToken();
  //     //You can print them to see the full objects
  //     console.log(`myAccessToken: ${JSON.stringify(accessToken)}`);
  //     console.log(`myJwt: ${jwt}`);
  //     this.tryit();
  //   });
  // };

  tryit = async () => {
    // const photo = {
    //   uri: this.state.VideoSource.uri,
    //   type: this.state.VideoSource.mime,
    //   name: "photo.jpg"
    // };
    // const form = new FormData();
    // form.append("ProfilePicture", photo);
    // fetch(Constants.API_USER + "me/profilePicture", {
    //   body: form,
    //   method: "PUT",
    //   headers: {
    //     "Content-Type": "multipart/form-data",
    //     Authorization: "Bearer " + user.token
    //   }
    // })
    //   .then(response => response.json())
    //   .catch(error => {
    //     console.log("ERROR ", error);
    //   })
    //   .then(responseData => {
    //     console.log("Success", responseData);
    //   })
    //   .done();
    const {
      bucket,
      aws_cognito_region,
      acsess_key,
      secret_access_key,
    } = awsexport;
    const access = new AWS.Credentials({
      accessKeyId: acsess_key,
      secretAccessKey: secret_access_key,
    });

    const s3 = new AWS.S3({
      credentials: access,
      region: aws_cognito_region,
    });
    var key = `public/post/${this.state.username}/${this.state.VideoSource.name}`;
    const s3Url = await s3.getSignedUrl('putObject', {
      Bucket: bucket,
      Key: key,
      ContentType: this.state.VideoSource.mime,
    });
    console.log(this.state.VideoSource, s3Url);
    RNFetchBlob.fetch(
      'PUT',
      s3Url,
      {'Content-Type': this.state.VideoSource.type},
      Platform.OS === 'ios'
        ? RNFetchBlob.wrap(this.state.VideoSource.uri)
        : `RNFetchBlob-file://${this.state.VideoSource.uri}`,
    )
      .then((resp) => {
        console.log(resp.text());
      })
      .catch((err) => {
        console.log(err);
      });
  };

  testit = async (token) => {
    const {
      bucket,
      aws_cognito_region,
      acsess_key,
      secret_access_key,
    } = awsexport;
    var key = `public/post/${this.state.username}/${this.state.VideoSource.name}`;
    console.log(key, this.state.VideoSource);
    try {
      RNFetchBlob.fetch(
        'POST',
        `https://${bucket}.s3.${aws_cognito_region}.amazonaws.com/public/${key}`,
        {
          // dropbox upload headers
          Accept: 'application/json',
          Authorization: token,
          'Content-Type': 'multipart/form-data',
          // Change BASE64 encoded data to a file path with prefix `RNFetchBlob-file://`.
          // Or simply wrap the file path with RNFetchBlob.wrap().
        },
        [
          {
            name: 'information',
            filename: this.state.VideoSource.name,
            // use custom MIME type
            type: this.state.VideoSource.mime,
            // upload a file from asset is also possible in version >= 0.6.2
            data: RNFetchBlob.wrap(this.state.VideoSource.uri),
          },
        ],
      )
        .uploadProgress((written, total) => {
          console.log('uploaded', written / total);
        })
        .then((res) => {
          console.log(res.text());
        })
        .catch((err) => {
          console.log(err);
        });
      // console.log(result)
    } catch (e) {
      console.error('Error', e);
    }
  };