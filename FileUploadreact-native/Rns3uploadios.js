try {
    var type = this.state.VideoSource.name.split('.');
    type = type[1];
    const file = {
      // `uri` can also be a file system path (i.e. file://)
      uri: this.state.VideoSource.uri,
      name: this.state.VideoSource.name,
      type: type,
    };
  const {
    bucket,
    aws_cognito_region,
    acsess_key,
    secret_access_key,
  } = awsexport;
  var key = `public/post/${this.state.username}/${this.state.VideoSource.name}`;
    const options = {
      keyPrefix: `public/post/${this.state.username}/`,
      bucket: bucket,
      region: aws_cognito_region,
      accessKey: acsess_key,
      secretKey: secret_access_key,
      successActionStatus: 201,
    };
    try {
  RNS3.put(file, options).then((response) => {
    if (response.status == 201) {
      const url = `https://${bucket}.s3.${aws_cognito_region}.amazonaws.com/${key}`;
      var mypost = {
        authorId: this.state.userdata.id,
        content: this.state.posttext,
        postVideo: url,
        userId: this.state.username,
      };
      this.props
        .PostCreate({
          variables: mypost,
        })

        .then(({data}) => {
          console.log(data);
          this.setState({loader: false});

          if (this.state.userdata.userType == 'PRO') {
            this.props.navigation.navigate('DashBoardProf');
          } else {
            this.props.navigation.navigate('DashBoardOrg');
          }
        })
        .catch((err) => {
          console.log(`${JSON.stringify(err)}`);
        });
        } else {
          alert('Something went wrong.');
        }
      });
    } catch (err) {
      console.log(err);
    }
  } catch (e) {
    console.log(e);
    alert('Something went wrong.');
  }